# coding: utf-8

"""Projet Goujons - Exigences 1,2,3"""

## Importation de modules

import numpy as np
import datetime

## Définitions de fonctions

def totalimages(): # Pour éviter l'utilisation de readlines(), peu efficace lorsque le fichier est gros
    """Compte le nombre total d'images contenues dans la vidéo"""
    
    with open("trajectories.txt","r") as trajectoires:
        n = 0
        for ligne in trajectoires:
            n += 1
    return(n)


def listeidTracker(nombrepoissons):
    """Récupère les coordonées de chaque poisson dans une liste Coord données sous cette forme : Coord=[[X1,Y1],[X2,Y2]...]"""
    
    totalimg = totalimages()

    dX = {} # Création de deux dictionnaires contenant les variables des positions
    dY = {}

    for numéro in range(1,nombrepoissons+1): # Déclaration des variables de position de chaque poisson
        dX["X{0}".format(numéro)] = []
        dY["Y{0}".format(numéro)] = []
    
    Coord = []

    for listevide in range(nombrepoissons): # Forme de Coord : Coord=[[X1,Y1],[X2,Y2]...]
        Coord.append([[],[]])
    
    colonnesidTracker = []
    
    for i in range (nombrepoissons*3): # Nombre de données utiles (positions fournies dans "trajectories.txt")
        colonnesidTracker.append(i)
    del colonnesidTracker[2::3] # On enlève les colonnes ProbaId à partir de la 3ème colonne (2 en informatique puisqu'on commence à 0)

    with open("trajectories.txt","r") as trajectoires:
        lireligne=trajectoires.readlines()

    for k in range(0,len(colonnesidTracker)-1,2): # Affectation des résultats dans les variables
        for j in range(1,totalimg):
            dX["X%s" % int((k/2)+1)].append((lireligne[j]).split("\t")[colonnesidTracker[k]]) # Création des listes Xs contenant les coordonnées X du poisson "s" pour chaque image
            dY["Y%s" % int((k/2)+1)].append((lireligne[j]).split("\t")[colonnesidTracker[k+1]]) # Création des listes Ys contenant les coordonnées Y du poisson "s" pour chaque image

    for u in range(1,nombrepoissons+1): # Création de la MEGA-liste Coord
        dX["X%s" % u] = list(map(float, dX["X%s" % u])) # Solution provisoire, les listes Xs et Ys des poissons "s" contiennet des chaînes de caractères, on convertit ces chaînes en "nombre décimaux". Ceci permettant de prendre en compte les "NaN"
        dY["Y%s" % u] = list(map(float, dY["Y%s" % u]))
        Coord[u-1][0].append(dX["X%s" % u])
        Coord[u-1][1].append(dY["Y%s" % u])
    
    return(Coord)

def tempsécoulé(image,ips):
    tempssec=image/ips
    tempsmin=tempssec/60
    tempsheure=tempsmin/60
    
    return(tempssec,tempsmin,tempsheure)
"""def zones(coeffrésolution): # X = [Haut gauche, Haut droit, Bas droit, Bas gauche] en pixels, à titre indicatif, coeffrésolution on verra...
    
    AX,AY = [x for x in range(309,828)],[x for x in range(5,541)]
    BX,BY = [x for x in range(827,1348)],AY
    CX,CY = [x for x in range(1347,1867)],AY
    DX,DY = AX,[x for x in range(540,1077)]
    EX,EY = BX,[x for x in range(540,1077)]
    FX,FY = CX,[x for x in range(540,1077)]

    """
    A=[(309,5),(827,5),(827,540),(309,540)]
    B=[(827,5),(1347,5),(1347,540),(827,540)]
    C=[(1347,5),(1866,6),(1866,540),(1347,540)]
    D=[(309,540),(827,540),(827,1076),(309,1076)]
    E=[(827,540),(1347,540),(1347,1076),(827,1076)]
    F=[(1347,540),(1866,540),(1866,1076),(1347,1076)]
    """

    return([[AX,AY],[BX,BY],[CX,CY],[DX,DY],[EX,EY],[FX,FY]])

def lavisite(MEGALISTE):

    nombrepoissons=len(MEGALISTE)

    A,B,C,D,E,F=zones(coeffrésolution)[0],zones(coeffrésolution)[1],zones(coeffrésolution)[2],zones(coeffrésolution)[3],zones(coeffrésolution)[4],zones(coeffrésolution)[5] # HEADS WILL ROLL

    dvisite = {} # Création d'un dictionnaire contenant les variables du nombre total de visites pour chaque poissons

    for numéro in range(1,nombrepoissons+1): # Déclaration des variables de position de chaque poisson
        dvisite["visite{0}".format(numéro)] = 1


    for i in range(1,nombrepoissons+1): # On s'intéresse au poisson i
        if MEGALISTE[i][0][0] in A[0] and MEGALISTE[i][1][0] in A[1]: # Zone dans laquelle se situe le poisson i au départ
                print("Le poisson",i,"est dans la zone A")

                    while MEGALISTE[i][0][j] in A[0] and MEGALISTE[i][1][j] in A[1]: # Tant que le poisson i est dans A alors on passe à l'image j suivante
                        j += 1
                    dvisite["visite%s" % i] += 1 # Dès que ce n'est plus le cas, alors il viste une nouvelle zone, on recommence l'opération pour les zones restantes

                    if MEGALISTE[i][0][j] in B[0] and MEGALISTE[i][1][j] in B[1]:

                        while MEGALISTE[i][0][j] in B[0] and MEGALISTE[i][1][j] in B[1]:
                        j += 1
                        dvisite["visite%s" % i] += 1

                        if MEGALISTE[i][0][j] in C[0] and MEGALISTE[i][1][j] in C[1]:

                            while MEGALISTE[i][0][j] in C[0] and MEGALISTE[i][1][j] in C[1]:
                                j += 1
                            dvisite["visite%s" % i] += 1

                            if MEGALISTE[i][0][j] in D[0] and MEGALISTE[i][1][j] in E[1]:

                                while MEGALISTE[i][0][j] in D[0] and MEGALISTE[i][1][j] in E[1]:
                                    j += 1
                                dvisite["visite%s" % i] += 1
                                

                                if MEGALISTE[i][0][j] in E[0] and MEGALISTE[i][1][j] in E[1]:

                                    while MEGALISTE[i][0][j] in E[0] and MEGALISTE[i][1][j] in E[1]:
                                        j += 1
                                    dvisite["visite%s" % i] += 1

                                    if MEGALISTE[i][0][j] in F[0] and MEGALISTE[i][1][j] in F[1]:

                                        while MEGALISTE[i][0][j] in F[0] and MEGALISTE[i][1][j] in F[1]:
                                            j += 1
                                        dvisite["visite%s" % i] += 1
                                
                                elif MEGALISTE[i][0][0] in F[0] and MEGALISTE[i][1][0] in F[1]:

                                    while MEGALISTE[i][0][j] in F[0] and MEGALISTE[i][1][j] in F[1]:
                                        j += 1
                                    dvisite["visite%s" % i] += 1

                                    if MEGALISTE[i][0][j] in F[0] and MEGALISTE[i][1][j] in F[1]:

                                        while MEGALISTE[i][0][j] in F[0] and MEGALISTE[i][1][j] in F[1]:
                                            j += 1
                                        dvisite["visite%s" % i] += 1



                                        
                                        

                            elif MEGALISTE[i][0][0] in E[0] and MEGALISTE[i][1][0] in E[1]:

                            elif MEGALISTE[i][0][0] in F[0] and MEGALISTE[i][1][0] in F[1]:
                                

                        elif MEGALISTE[i][0][0] in D[0] and MEGALISTE[i][1][0] in D[1]:

                        elif MEGALISTE[i][0][0] in E[0] and MEGALISTE[i][1][0] in E[1]:

                        elif MEGALISTE[i][0][0] in F[0] and MEGALISTE[i][1][0] in F[1]:


                    elif MEGALISTE[i][0][0] in C[0] and MEGALISTE[i][1][0] in C[1]:

                    elif MEGALISTE[i][0][0] in D[0] and MEGALISTE[i][1][0] in D[1]:

                    elif MEGALISTE[i][0][0] in E[0] and MEGALISTE[i][1][0] in E[1]:

                    elif MEGALISTE[i][0][0] in F[0] and MEGALISTE[i][1][0] in F[1]:

                    

        elif MEGALISTE[i][0][0] in B[0] and MEGALISTE[i][1][0] in B[1]:
                print("Le poisson",i,"est dans la zone B")

        elif MEGALISTE[i][0][0] in C[0] and MEGALISTE[i][1][0] in C[1]:
                print("Le poisson",i,"est dans la zone C")

        elif MEGALISTE[i][0][0] in D[0] and MEGALISTE[i][1][0] in D[1]:
                print("Le poisson",i,"est dans la zone D")

        elif MEGALISTE[i][0][0] in E[0] and MEGALISTE[i][1][0] in E[1]:
                print("Le poisson",i,"est dans la zone E")

        elif MEGALISTE[i][0][0] in F[0] and MEGALISTE[i][1][0] in F[1]:
                print("Le poisson",i,"est dans la zone F")"""


    
            
            
            
        

    

    
## Corps du programme

nombrepoissons = 5
coeffrésolution = 1 # on est sur du 1920x1080 (Full HD), pour du 3840x2160 (UHDTV-1) ce serait 4. Par hypothèse on considère que seule la résolution peut changer sans changer le format (16:9) et que les bande rouges sont disposées de la même façon
MEGALISTE = listeidTracker(nombrepoissons)
